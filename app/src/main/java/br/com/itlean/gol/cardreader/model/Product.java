package br.com.itlean.gol.cardreader.model;

import java.io.Serializable;

/**
 * Created by mikabrytu on 25/01/17.
 */

public class Product implements Serializable {

    String name, image;
    double price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
