package br.com.itlean.gol.cardreader.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.itlean.gol.cardreader.R;
import br.com.itlean.gol.cardreader.model.Product;

/**
 * Created by mikabrytu on 27/01/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private Context context;
    private List<Product> list;

    public HistoryAdapter(Context context, List<Product> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.inflate_history_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Product item = list.get(position);

        holder.title.setText(item.getName());
        holder.price.setText(String.valueOf(item.getPrice()));

        Picasso.with(context).load(item.getImage()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<Product> list) {
        this.list.clear();
        this.list.addAll(list);
        this.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView title, price;

        public ViewHolder(View view) {
            super(view);

            image = (ImageView) view.findViewById(R.id.image_history_item);
            title = (TextView) view.findViewById(R.id.title_history_item);
            price = (TextView) view.findViewById(R.id.label_history_price);
        }
    }

}
