package br.com.itlean.gol.cardreader.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import br.com.itlean.gol.cardreader.R;
import br.com.itlean.gol.cardreader.activity.CartActivity;
import br.com.itlean.gol.cardreader.activity.MainActivity;
import br.com.itlean.gol.cardreader.model.Product;

/**
 * Created by mikabrytu on 25/01/17.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private Context context;
    private List<Product> list;
    private MainActivity activity;

    private double totalPrice = 0.00;
    private List<Product> cartList;

    public ProductsAdapter(Context context, List<Product> list) {
        this.context = context;
        this.list = list;

        cartList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.inflate_main_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Product item = list.get(position);

        holder.name.setText(item.getName());
        holder.price.setText(String.valueOf(item.getPrice()));

        Picasso.with(context).load(item.getImage()).into(holder.logo);

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q = Integer.parseInt(holder.quantity.getText().toString());
                q++;
                totalPrice += item.getPrice();
                holder.quantity.setText(String.valueOf(q));

                cartList.add(item);

                double value = Math.round(totalPrice * 100.0) / 100.0;
                activity.updateResume(String.valueOf(value));
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int q = Integer.parseInt(holder.quantity.getText().toString());
                if (q > 0) {
                    q--;
                    totalPrice -= item.getPrice();
                    holder.quantity.setText(String.valueOf(q));

                    cartList.remove(item);

                    double value = Math.round(totalPrice * 100.0) / 100.0;
                    activity.updateResume(String.valueOf(value));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<Product> list) {
        this.list.clear();
        this.list.addAll(list);
        this.notifyDataSetChanged();
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public List<Product> getList() {
        return cartList;
    }

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout layout;
        ImageView logo;
        TextView name, price, quantity;
        Button plus, minus;

        ViewHolder(View view) {
            super(view);

            layout = (LinearLayout) view.findViewById(R.id.layout_products_item);
            logo = (ImageView) view.findViewById(R.id.image_products_item);
            name = (TextView) view.findViewById(R.id.title_products_item);
            price = (TextView) view.findViewById(R.id.label_products_item_price);
            plus = (Button) view.findViewById(R.id.button_products_item_plus);
            minus = (Button) view.findViewById(R.id.button_products_item_minus);
            quantity = (TextView) view.findViewById(R.id.label_products_item_quantity);
        }
    }

}
