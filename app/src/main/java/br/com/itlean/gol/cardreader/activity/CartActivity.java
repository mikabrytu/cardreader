package br.com.itlean.gol.cardreader.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.itlean.gol.cardreader.R;
import br.com.itlean.gol.cardreader.database.Database;
import br.com.itlean.gol.cardreader.model.Product;

public class CartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        double value = getIntent().getDoubleExtra("EXTRA_TOTAL", 0.00);
        TextView total = (TextView) findViewById(R.id.label_cart_total);
        total.setText("R$ " + String.valueOf(value));

        Button buy = (Button) findViewById(R.id.button_cart_finish_buy);
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                completeBuy();
            }
        });
    }

    private void completeBuy() {
        Database database = new Database(this);
        Bundle args = getIntent().getBundleExtra("EXTRA_BUNDLE");
        List<Product> list = (List<Product>) args.getSerializable("list");

        for (Product p : list) {
            database.addProduct(p);
        }

        showMessage();
    }

    private void showMessage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sucesso");
        builder.setMessage("Compra realizada com sucesso");
        builder.setPositiveButton("Finalizar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton("Histórico", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                startActivity(new Intent(CartActivity.this, HistoryActivity.class));
                finish();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
