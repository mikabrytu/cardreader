package br.com.itlean.gol.cardreader.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.itlean.gol.cardreader.model.Product;

/**
 * Created by mikabrytu on 27/01/17.
 */

public class Database extends SQLiteOpenHelper {

    private static int DATABASE_VERSION = 1;
    private static String DATABASE_NAME = "project_database";

    private String TABLE_PRODUCTS = "products_history";

    private String FIELD_ID = "id";
    private String FIELD_NAME = "name";
    private String FIELD_PRICE = "price";
    private String FIELD_IMAGE = "image";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String sql = "CREATE TABLE "+ TABLE_PRODUCTS +" ( " +
                FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                FIELD_NAME + " VARCHAR(255)," +
                FIELD_PRICE + " DOUBLE," +
                FIELD_IMAGE + " VARCHAR(255)" +
                ")";
        database.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i1) {
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
    }

    public void addProduct(Product product) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FIELD_NAME, product.getName());
        values.put(FIELD_PRICE, product.getPrice());
        values.put(FIELD_IMAGE, product.getImage());

        database.insert(TABLE_PRODUCTS, null, values);
        database.close();
    }

    public Product getProduct(int id) {
        SQLiteDatabase database = this.getReadableDatabase();

        String sql = "SELECT "+ FIELD_NAME +", "+ FIELD_PRICE +", "+ FIELD_IMAGE +" FROM "+ TABLE_PRODUCTS +" WHERE "+ FIELD_ID +" =?";
        Cursor cursor = database.rawQuery(sql, new String[] {String.valueOf(id)});

        Product product = new Product();
        product.setName(cursor.getString(0));
        product.setPrice(cursor.getDouble(1));
        product.setImage(cursor.getString(2));

        cursor.close();

        return product;
    }

    public List<Product> getHistory() {
        SQLiteDatabase database = this.getWritableDatabase();
        List<Product> list = new ArrayList<>();

        String sql = "SELECT * FROM " + TABLE_PRODUCTS;
        Cursor cursor = database.rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                Product product = new Product();

                product.setName(cursor.getString(1));
                product.setPrice(cursor.getDouble(2));
                product.setImage(cursor.getString(3));

                list.add(product);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return list;
    }

    public int updateHistory(Product product, int id) {
        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FIELD_NAME, product.getName());
        values.put(FIELD_PRICE, product.getPrice());
        values.put(FIELD_IMAGE, product.getImage());

        return database.update(TABLE_PRODUCTS, values, FIELD_ID + "=?", new String[] {String.valueOf(id)});
    }

    public void deleteProduct(int id) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_PRODUCTS, FIELD_ID + "=?", new String[] {String.valueOf(id)});
        database.close();
    }
}
