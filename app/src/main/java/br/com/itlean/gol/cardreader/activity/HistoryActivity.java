package br.com.itlean.gol.cardreader.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.itlean.gol.cardreader.R;
import br.com.itlean.gol.cardreader.adapter.HistoryAdapter;
import br.com.itlean.gol.cardreader.adapter.ProductsAdapter;
import br.com.itlean.gol.cardreader.database.Database;
import br.com.itlean.gol.cardreader.model.Product;

public class HistoryActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private List<Product> list;
    private HistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initFields();
        configFields();
        setListeners();

        prepareData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initFields() {
        recycler = (RecyclerView) findViewById(R.id.recycler_history);

        list = new ArrayList<>();
        adapter = new HistoryAdapter(this, list);
    }

    private void configFields() {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recycler.setLayoutManager(manager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(adapter);
    }

    private void setListeners() {
        //
    }

    private void prepareData() {
        Database database = new Database(this);
        adapter.updateList(database.getHistory());
    }

}
