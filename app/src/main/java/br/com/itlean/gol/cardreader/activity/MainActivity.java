package br.com.itlean.gol.cardreader.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.itlean.gol.cardreader.R;
import br.com.itlean.gol.cardreader.adapter.ProductsAdapter;
import br.com.itlean.gol.cardreader.model.Product;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private TextView resume;

    private List<Product> list;
    private ProductsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initFields();
        configFields();
        setListeners();

        prepareData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart:
                Bundle args = new Bundle();
                args.putSerializable("list", (Serializable) adapter.getList());

                Intent intent = new Intent(MainActivity.this, CartActivity.class);
                intent.putExtra("EXTRA_TOTAL", adapter.getTotalPrice());
                intent.putExtra("EXTRA_BUNDLE", args);
                startActivity(intent);

                return true;
            case R.id.action_history:
                startActivity(new Intent(this, HistoryActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initFields() {
        recycler = (RecyclerView) findViewById(R.id.recyccler_main);
        resume = (TextView) findViewById(R.id.label_main_resume);

        list = new ArrayList<>();
        adapter = new ProductsAdapter(this, list);
    }

    private void configFields() {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recycler.setLayoutManager(manager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.setAdapter(adapter);

        adapter.setActivity(this);
    }

    private void setListeners() {
        //
    }

    private void prepareData() {
        List<Product> productList = new ArrayList<>();

        Product product = new Product();

        product.setName("Picanha Bovina Resfriada Montana Peça à Vácuo");
        product.setPrice(104.85);
        product.setImage("http://mambo.vteximg.com.br/arquivos/ids/179762-95-95/131462.jpg");
        productList.add(product);

        product = new Product();
        product.setName("Carne Moída de Acém Bovina Resfriada Bandeja");
        product.setPrice(9.24);
        product.setImage("http://mambo.vteximg.com.br/arquivos/ids/183480-95-95/194819_40061.jpg");
        productList.add(product);

        product = new Product();
        product.setName("Mistura para Bolo Petit Gateau Yoki Caixa 450g");
        product.setPrice(10.95);
        product.setImage("http://mambo.vteximg.com.br/arquivos/ids/176376-95-95/192141.jpg");
        productList.add(product);

        product = new Product();
        product.setName("Cerveja Uruguaia Patrícia Long Neck 300ml");
        product.setPrice(11.69);
        product.setImage("http://mambo.vteximg.com.br/arquivos/ids/182762-95-95/201050.jpg");
        productList.add(product);

        product = new Product();
        product.setName("Farinha de Mandioca Torrada Yoki 500g");
        product.setPrice(5.45);
        product.setImage("http://mambo.vteximg.com.br/arquivos/ids/163961-95-95/7891095300389_farinha_de_mandioca_torrada_yoki_500g_01.jpg");
        productList.add(product);

        product = new Product();
        product.setName("Leite Longa Vida Integral com tampa Ninho 1 Litro");
        product.setPrice(3.98);
        product.setImage("http://mambo.vteximg.com.br/arquivos/ids/171355-95-95/7891000066560_-leite-integral_-ninho-1l--1-.jpg");
        productList.add(product);

        product = new Product();
        product.setName("Filé de Peito de Frango Congelado Sadia 1kg");
        product.setPrice(9.98);
        product.setImage("http://mambo.vteximg.com.br/arquivos/ids/157246-95-95/7893000005945_file-de-peito_frango-congelado_sadia_1kg_02.jpg");
        productList.add(product);

        product = new Product();
        product.setName("Chester Assa Fácil Congelado Perdigão");
        product.setPrice(47.92);
        product.setImage("http://mambo.vteximg.com.br/arquivos/ids/183434-95-95/135010.jpg");
        productList.add(product);

        adapter.updateList(productList);
    }

    public void updateResume(String value) {
        resume.setText("R$ " + value);
    }

}
